## Initial obsservation 
> 1. Including "Unknown" 2499 distinct __*Villages*__
> 1. No pattern found in __*Group Id*__
> 1. Clear pattern of decreasing count as the __*Member Id*__ increases not clear why?
> 1. Branch ID has the same distribution of __*Branch Name*__
> 1. 985 unique __*Circle names*__
> 1. A time type element found in __*Loan Amount*__
> 1. Three __*Product types*__ : 
    >    * Greenway Jumbo Stove
    >    * Solar Light Selco
    >    * Cook Stove GSSV3
> 1. __*Name of the model*__ is as follows:
    >    * Cook Stove GSSV3 : GSSV3
    >    * Greenway Jumbo Stove GJS1 : GJS1
    >    * Solar Light Selco : other 11 products
> 1. __*Product details*__ gives 4 unique product under Solar Light Selco as the rest is missing
    >    * LED Light
    >    * CFL
    >    * Fan
    >    * COMBI Light
> 1. Two __*Manufactureres*__ : Greenway Grameen Infra (Stoves) and SELCO (Solar Lights)
> 1. __*Loan account number*__ has 50 duplicate records, not clear why?
> 1. __*Loan ID*__ explains the duplication above, loan_id has loan account number in it along with a prefix
> 1. __*Household Id*__ is a combination of Group Id and Member Id
> 1. __*Product_id*__ has two codes for NaN
> 1. Data is from Jun 2017 to Apr 2019
> 1. __*Loan Application Date*__ has a row with data from a string column
> 1. __*Document signed Date*__ is same as __*Loan Application Date*__
> 1. Only one __*Partner ID*__ and __*Institution*__

---
## Insights from basic plot and filtering
> 1. __*Loan Amount*__ 
    >    * On an average the loan is for ~5k with 50% of spread between ~2k and ~9k
    >    * 7 cases where Loan amount is not rounded of nearest tenth digit
> 1. __*Loan Application Date*__ and __*Document signed Date*__
    >    * On an average the difference between application and installation is ~4 days
    >    * 25th - 75th percentile is delay of 2-6 days
> 1. Top 10 __*Villages*__ where time between application and installation is the highest:
    >    * kotthegala
    >    * chikkakereyur village panchayatha
    >    * ingalagundi
    >    * doddadasenahalli
    >    * kumbalagodu gollahalli
    >    * ikkanuru
    >    * appenahalli
    >    * yakatpur
    >    * sihi neerina kola
    >    * harave
> 1. Top 10 __*Branch*__ where time between application and installation is the highest:
    >    * heggadevanakote
    >    * hirekerur
    >    * hunsur
    >    * shidlaghatta
    >    * hiriyur
    >    * ramanagara
    >    * kudligi
    >    * bidar
    >    * devanahalli
    >    * nelamangala
> 1. Top 10 __*Circle*__ where time between application and installation is the highest:
    >    * madapura
    >    * hirekerur
    >    * karnakuppe
    >    * shidlaghatta a
    >    * hariyabbe
    >    * kumbalagodu
    >    * manhalli
    >    * gudekote
    >    * induvalu
    >    * nallur
> 1. __*Product Type*__
    >    * Solar Light Selco : ~22 days
    >    * Greenway jumbo stove gjs1 : ~12.5 days
    >    * Cook stove gssv3 : ~10.7 days
> 1. __*Name of Model*__
    >    * eh2hls : ~58.4 days
    >    * sb4hls : ~20 days
    >    * eh4hls : ~17.7 days
    >    * sb2hls : ~17.3 days
    >    * bh4hls : ~13 days
    >    * sb9hls : ~13 days
    >    * gjs1 : ~12.5 days
    >    * gssv3 : ~10.7 days
    >    * s14hls : ~10 days
    >    * s10hls : ~9.5days
    >    * sb6hls : ~9 days
> 1. __*Manufacturers*__
    >    * Selco : ~22 days
    >    * Greenway Grameen Infra : ~11.7 days
> 1. __*Installation Date*__ was before Loan and Signing for these 4 cases, which is incorrect
for these cases, switching installation date with signed document assuming data entry error

---
## Insights from graphs
> 1.  Month wise __*Installation*__ of products
    >       * Apart from these bh4hls, s15hls, sb6hls, and sb9hls all are in demand through out the year
    >       * April and May always have the least products installed
    >       * September and October sees increase in demand for installation
> 1. __*Village*__ wise installation of products
    >       * For Villages with more than 4 installations in the time duration, maximum installtion is 9
> 1. __*Village*__ and __*Loan amount*__
    >       * Top spenders on loan
                >          * sirigere
                >          * soravanahalli
                >          * bendekere
                >          * s.m krishnagara
                >          * nippani
                >          * gubbi
                >          * hirehalli village panchayatha
                >          * hosur
                >          * marasanige
                >          * hampapura  village panchayatha
    >       * Least spenders on loan
                >          * t.bettahalli
                >          * guttahalli
                >          * thungani
                >          * hunkunda
                >          * dharmaraya nagara
                >          * gummanakolli
                >          * kennalu
                >          * amachavadi
                >          * malangi
                >          * hankere
> 1. Month and __*Loan Applied Date*__
    >       * Highest during 1st and 3rd Quarter
> 1. Month and __*Installation Date*__
    >       * No Installation during the month of August
> 1. __*Village*__ and Delay
    >       * Top 10  Villages with the most delay on an average of more than 15 days
                >          * ingalagundi
                >          * kotthegala
                >          * chikkakereyur village panchayatha
                >          * doddadasenahalli
                >          * kumbalagodu gollahalli
                >          * appenahalli
                >          * yakatpur
                >          * harave
                >          * sihi neerina kola
                >          * anekal (rural)
> 1. __*Village*__ with most installed __*Name of model*__ 
    >       * sh4hls 
                >          * sirigere
                >          * ajri
                >          * arala
                >          * karagadde
                >          * amparu
                >          * anekere
                >          * bagivalu
                >          * bajaguru
                >          * belve
                >          * honnavalli
    >       * sb9hls 
                >          * almatti
                >          * badami
                >          * hanagodu
                >          * navalgund  c
                >          * nelyady
                >          * t.nulenuru
                >          * venkatagiri
    >       * sb8hls  
                >          * anaberu
                >          * avarse
                >          * belve
                >          * devi nagara
                >          * hirebidanal
                >          * hosur
                >          * 34 kudi
                >          * 74 ulluru
                >          * balya
                >          * belle
    >       * sb6hls   
                >          * hadalageri
                >          * hakladi
                >          * hosahalli
                >          * karakuchi
                >          * kuchoor
                >          * mattur
    >       * sb4hls    
                >          * b.kanaboor
                >          * soravanahalli
                >          * aduvalli
                >          * devadana
                >          * gangavathi
                >          * agumbe (talluru)
                >          * aramanekoppa
                >          * avargere
                >          * bairidevarakoppa
                >          * basavana bagevadi (tmc)
    >       * sb2hls     
                >          * hampapura  village panchayatha
                >          * chennipuramole
                >          * karagunda
                >          * rangapura
                >          * bendekere
                >          * hanagodu
                >          * hasudi-a
                >          * huduguru
                >          * mahadevapura
                >          * old hubli
    >       * s15hls      
                >          * amaramudnooru
                >          * badagabelluru
                >          * kanasinakatte(sidlipura)
                >          * marasanige
                >          * s.m krishnagara
    >       * s14hls       
                >          * bukkambudi
                >          * devachalla
                >          * gubbi
                >          * gudigondanahalli
                >          * heggala
                >          * horanadu
                >          * jalsoor
                >          * karkeshwar
                >          * kodiyala
                >          * kolake gadde house, kavalamooduru village,kavalakatte.
    >       * s10hls        
                >          * kattimallenahalli
                >          * adki
                >          * bijkal
                >          * d/o. lingappa gowda, kamarkaje  house, kombaru  village, puttur
                >          * gunduri
                >          * heggere
                >          * kukkala
                >          * kushtagi-b.b nagar
                >          * morageri
                >          * mulluru
    >       * gssv3         
                >          * shidlaghatta rural
                >          * soravanahalli
                >          * y. hunasenahalli
                >          * hulikere
                >          * kasaba
                >          * kodiyala
                >          * t. gollahalli
                >          * hatti
                >          * k shettihalli
                >          * srirangapatna
    >       * gjs1          
                >          * bijapur cmc
                >          * muddebihal tmc
                >          * talikota tmc
                >          * challakere
                >          * mysore city corporation
                >          * devara hippargi
                >          * hospete
                >          * ranebennur f
                >          * hattargi
                >          * humnabad rural
    >       * eh4hls           
                >          * bendekere
                >          * ingali
                >          * hirehalli village panchayatha
                >          * adagur
                >          * aralikoppa
                >          * chagachagere
                >          * challakere
                >          * konanur
                >          * thuruvekere nagara
                >          * akkalasandra
    >       * eh2hls            
                >          * nippani
                >          * y.t. halli
                >          * halsuru
                >          * mallipattana
                >          * agoli
                >          * banavara
                >          * belluru
                >          * hannikeri
                >          * jajur
                >          * kodihalli
    >       * bh4hls             
                >          * balkuru
                >          * karkeshwar
                >          * marle
                >          * mudradi
                >          * ranganathapura
> 1. Year vs __*Loan Amount*__ 
    >       * 2018 is the highest
> 1. Year vs __*Installation count*__ 
    >       * 2018 is the highest
> 1. There is no hierarchy between __*Village*__, __*Branch*__ and __*Circle*__ name
> 1. __*Branch*__ and __*Loan amount*__
    >       * Top spenders on loan
                >          * arasikere
                >          * turuvekere
                >          * koppa
                >          * sullia
                >          * hubli
                >          * chikodi
                >          * kundapur
                >          * udupi
                >          * moodigere
                >          * arkalgud
                >          * chitradurga
                >          * hunsur
                >          * heggadevanakote
                >          * bantwal
                >          * honnavar
    >       * Least spenders on loan
                >          * mudhol
                >          * mudalagi
                >          * manvi
                >          * bagepalli
                >          * aland
                >          * yadgir
                >          * chikkaballapura
                >          * gulbarga
                >          * rayachur
                >          * yelendur
                >          * afzalpur
                >          * kudligi
                >          * harappanahalli
                >          * mangalore urban
                >          * gauribidanur
> 1. __*Branch*__ and Delay
    >       * Top 10  Villages with the most delay on an average of more than 15 days
                >          * hirekerur
                >          * kudligi
                >          * hunsur
                >          * pandavapura
                >          * heggadevanakote
                >          * shrirangapatna
                >          * kumble-kasargud
                >          * basavakalyan
                >          * kinnigoli
                >          * malavalli
> 1. __*Circle*__ and __*Loan amount*__
    >       * Top spenders on loan
                >          * guthigaru
                >          * guthigaru
                >          * kasaba
                >          * kasaba
                >          * balehonnur
                >          * kasaba
                >          * sirigere
                >          * kondajji
                >          * virajpet
                >          * gabbur
    >       * Least spenders on loan
                >          * bagepalli b
                >          * kudlur
                >          * bannuru
                >          * nagaragere
                >          * chimangala
                >          * bannuru
                >          * thalagavadi
                >          * venkatagiri kote
                >          * bidadi
                >          * mishrikoti
> 1. __*Circle*__ and Delay
    >       * Top 10  Villages with the most delay on an average of more than 15 days
                >          * hirekerur
                >          * kumbalagodu
                >          * gudekote
                >          * madapura
                >          * eachanal
                >          * karnakuppe
                >          * morageri
                >          * k.honnalagere
                >          * aralakuppe
                >          * kaniyoor
> 1. Same implemented for combination __*Village*__, __*Circle*__, and __*Branch*__

> ## Other insights
    >  1. Delay in installation, though low, reduces as loan size increases
    >  1. Row number 4113 was entered incorrectly in multiple ways: __*Loan amount*__,  __*Loan application date*__ , __*Village name*__
    > 1. 4 cases of loan applied after installation found


---
##  Data Mapped
| Product Detail | Product ID|
|---|---|
| 4 COMBI Light | 50 |
| 12 CFL + 2 Fan | 42 |
|  2 LED Light | 35 |
| 10 CFL | 40 |
| NaN | 38 |
| NaN | 37 |
| 2 CFL | 43 |
| 4 LED Light | 36 |
| 10 CFL + 2 Fan | 41 |
| 8 CFL | 47 |
| 6 CFL | 46 |
| 9 CFL | 48 |
| 2 CFL + 1 Fan | 23 |

| Product | Count |
|---|---|
| LED | 1434 |
| Fan | 49 |
| CFL | 2091 |
| Combi | 720 |
| total | 4294 |

| Branch ID | Branch Name |
| --- | --- |
| 2 | Belthangady |
 | 3 | Karkala |
 | 4 | Sullia |
 | 5 | Puttur |
 | 6 | Moodigere |
 | 7 | Mangalore Urban |
 | 8 | KinniGoli |
 | 9 | Karwar |
 | 10 | Bantwal |
 | 11 | Udupi |
 | 12 | Kundapur |
 | 13 | Dandeli |
 | 14 | Sirsi |
 | 15 | Hosanagar |
 | 16 | Kumta |
 | 17 | Honnavar |
 | 19 | Tarikere |
 | 20 | Kadur |
 | 21 | Tirthahalli |
 | 22 | Koppa |
 | 23 | Shimoga |
 | 24 | Sagar |
 | 25 | Sorab |
 | 26 | Shikarpur |
 | 27 | Chikamagalur |
 | 28 | Virajpet |
 | 29 | Somwarpet |
 | 30 | Dharwad |
 | 31 | Hubli |
 | 32 | Navalgunda |
 | 33 | Shiggon |
 | 34 | Ranebennur |
 | 35 | Haveri |
 | 36 | Rona |
 | 37 | Shirahatti |
 | 38 | gadaga |
 | 39 | Yellapur-Mundgod |
 | 40 | Bhadravathi |
 | 41 | Athani |
 | 42 | Raybag |
 | 43 | Chikodi |
 | 44 | Hukkeri |
 | 45 | Gokak |
 | 46 | Ramdurg |
 | 47 | Saundatti |
 | 48 | Bailahongal |
 | 49 | Belgaum |
 | 50 | Khanapur |
 | 51 | Pavagada |
 | 52 | Sira |
 | 53 | Koratagere |
 | 54 | Chiknayakanahalli |
 | 55 | Tiptur |
 | 56 | Turuvekere |
 | 57 | Kunigal |
 | 58 | Gubbi |
 | 59 | Tumkur |
 | 60 | Madhugiri |
 | 61 | Hirekerur |
 | 62 | Hanagal |
 | 63 | Heggadevanakote |
 | 64 | Nanjanagudu |
 | 65 | Arkalgud |
 | 66 | Thirumakodlu-Narasipura |
 | 67 | Mysore |
 | 68 | Krishnarajnagara |
 | 69 | Hunsur |
 | 70 | Kollegal |
 | 71 | Chamarajanagar |
 | 72 | Gundlupet |
 | 73 | Yelendur |
 | 74 | Challakere |
 | 75 | Hosadurga |
 | 76 | Hiriyur |
 | 77 | Holalkere |
 | 78 | Chitradurga |
 | 79 | Molakalmuru |
 | 80 | Kushtagi |
 | 81 | Yelburga |
 | 82 | Gangavathi |
 | 83 | Koppal |
 | 84 | Davanagere |
 | 85 | Honnali |
 | 86 | Harappanahalli |
 | 87 | Jagalur |
 | 88 | Channagere |
 | 89 | Harihara |
 | 90 | Kumble-Kasargud |
 | 91 | Belur |
 | 92 | Arasikere |
 | 93 | Channarayapatna |
 | 94 | Holenarasipura |
 | 95 | Krishnarajapete |
 | 96 | Pandavapura |
 | 97 | Mandya |
 | 98 | Maddur |
 | 99 | Shrirangapatna |
 | 100 | Malavalli |
 | 101 | Nagamangala |
 | 102 | Huvinahadagali |
 | 103 | Hagaribommanahalli |
 | 104 | Sandoor |
 | 105 | Hosapete |
 | 106 | Bellary |
 | 107 | Kudligi |
 | 108 | Siruguppa |
 | 110 | Manvi |
 | 111 | Rayachur |
 | 112 | Lingsugar |
 | 113 | Sindhanur |
 | 114 | Doddaballapur |
 | 115 | Devanahalli |
 | 116 | Nelamangala |
 | 117 | Hosakote |
 | 119 | Mudhol |
 | 120 | Bagalakote |
 | 121 | Badami |
 | 122 | Hungunda |
 | 123 | Hasana |
 | 124 | Sakleshpura |
 | 125 | Gauribidanur |
 | 127 | Bagepalli |
 | 128 | Chikkaballapura |
 | 129 | Shidlaghatta |
 | 130 | Chintamani |
 | 131 | Kolar |
 | 132 | Bangarpete |
 | 133 | Mulubagilu |
 | 134 | Malur |
 | 135 | Shrinivasapura |
 | 136 | Ramanagara |
 | 137 | Channapatna |
 | 138 | Kanakapura |
 | 139 | Magadi |
 | 140 | Piriyapatna |
 | 141 | Basavakalyan |
 | 142 | Homnabad |
 | 143 | Bidar |
 | 144 | Aurad |
 | 145 | Bhalki |
 | 146 | Bijapur |
 | 147 | Basavan Bagevadi |
 | 148 | Muddebihal |
 | 149 | Indi |
 | 150 | Sindagi |
 | 151 | Aland |
 | 152 | Gulbarga |
 | 153 | Afzalpur |
 | 154 | Chincholi |
 | 155 | Jevargi |
 | 156 | Chitapur |
 | 157 | Sedam |
 | 158 | Shorapur |
 | 159 | Shahpur |
 | 160 | Yadgir |
 | 161 | Kundagol |
 | 2513 | Anekal |
 | 2568 | Kundapur B |
 | 2573 | Mudalagi |
 | 2574 | Kittur |
